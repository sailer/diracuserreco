#!/bin/env python
"""script to do run Mokka like for the CLIC_ILD transformations"""
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()


def run():
  from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import Mokka #, Marlin, OverlayInput
  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from DIRAC.Core.Utilities.List import breakListIntoChunks
  
  dIlc = DiracILC()

  ###################################################################################
  listOfInputFiles = [
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_100.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_101.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_102.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_103.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_104.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_105.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_106.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_107.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_108.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_11.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_110.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_111.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_113.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_114.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_115.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_116.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_117.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_12.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_120.stdhep',
    '/ilc/prod/clic/1.4tev/qq_ln/gen/00002157/000/qq_ln_gen_2157_121.stdhep',
  ] # FIXME, this needs to be the list of LFNs for the input data
  filesPerChunk = 1000
  # the trialNumber is for different trials in case one goes wrong
  trialNumber = 1

  for index, chunk in enumerate(breakListIntoChunks(listOfInputFiles, filesPerChunk)):
    # %n will be replaced by jobIndex
    outputFileName = "qqlnu_nopol_1.4tev_SIM.slcio"
    # the /1/ is for different trials in case one goes wrong
    outputPath = "qqlnu_nopol_1.4tev/SIM/%s/%02d" % (trialNumber, index)

    job = UserJob()
    job.setJobGroup("qqlnu_sim_%s" % trialNumber)
    job.setName("qqlnu_sim_%n")  # %n will be replaced by the task number
    job.setOutputSandbox("*.log")

    job.setSplitInputData(chunk, numberOfFilesPerJob=1)
    job.setSplittingStartIndex(index * filesPerChunk + 1)
    job.setOutputData(outputFileName, OutputPath=outputPath, OutputSE="CERN-DST-EOS")

    mo = Mokka()
    mo.setVersion('0706P08')
    mo.setNbEvts(100)
    mo.setSteeringFile("clic_ild_cdr.steer")
    mo.setOutputFile(outputFileName)

    job.append(mo)
    job.submit(dIlc)

if __name__ =="__main__":
  run()