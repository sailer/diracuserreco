#!/bin/env python
"""script to do run Mokka like for the CLIC_ILD transformations"""
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine()


def run():
  from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin, OverlayInput
  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from DIRAC.Core.Utilities.List import breakListIntoChunks
  
  dIlc = DiracILC()

  ###################################################################################
  listOfInputFiles = [
  ] # FIXME, this needs to be the list of LFNs for the input data
  filesPerChunk = 1000
  enableOverlay = True
  # the trialNumber is for different trials in case one goes wrong
  trialNumber = 1
  
  for index, chunk in enumerate(breakListIntoChunks(listOfInputFiles, filesPerChunk)):
    # %n will be replaced by jobIndex
    outputFileName = "qqlnu_nopol_1.4tev_DST%s.slcio" % ('_Over' if enableOverlay else '')
    outputPath = "qqlnu_nopol_1.4tev/DST/%s/%02d" % (trialNumber, index)

    job = UserJob()
    job.setName("qqlnu_rec_%n")  # %n will be replaced by the task number
    job.setJobGroup("qqlnu_rec_%s" % trialNumber)
    job.setOutputSandbox("*.log")
    job.setInputSandbox(['clic_ild_cdr_steering_overlay_1400.0.xml',
                         'clic_ild_cdr_steering.xml',
                         ])

    job.setSplitInputData(chunk, numberOfFilesPerJob=1)
    job.setSplittingStartIndex(index * filesPerChunk + 1)
    job.setOutputData(outputFileName, OutputPath=outputPath, OutputSE="CERN-DST-EOS")

    if enableOverlay:
      overlay = OverlayInput()
      overlay.setMachine("clic_cdr")
      overlay.setEnergy(1400.)
      overlay.setBkgEvtType("gghad")
      overlay.setDetectorModel("CLIC_ILD_CDR")
      overlay.setBXOverlay(60)
      overlay.setNumberOfSignalEventsPerJob(100)
      overlay.setNbEvts(100)
      overlay.setGGToHadInt(1.3)  # When running at 1.4TeV
      job.append(overlay)
    
    ma = Marlin()
    ma.setVersion('v0111Prod')
    ma.setNbEvts(100)
    if enableOverlay:
      ma.setSteeringFile("clic_ild_cdr_steering_overlay_1400.0.xml")
      ma.setGearFile('clic_ild_cdr.gear')
    else:
      ma.setSteeringFile("clic_ild_cdr_steering.xml")
      ma.setGearFile('clic_ild_cdr.gear')
    ma.setOutputFile(outputFileName)

    job.append(ma)
    
    job.submit(dIlc)

if __name__ =="__main__":
  run()